﻿jQuery(document).ready(function ($) {
    setDecoAngles();
    setTabs();
    setImgTransition();

    try {
        setIsotop();
    } catch (e) {
        console.log('smth gone wrong with isotop')
    }

    try {
        //if ($("body").hasClass('secondary')) return;
        $('#fullpage').fullpage({
            anchors: ['home', 'products', 'projects', 'partners', 'contacts'],
            menu: '#menu_items',
            autoScrolling: false,
            normalScrollElements: '#products',
            afterLoad: function (anchorLink, index) {
                if (index !== '1') {
                    $('.nav_wrapper').addClass('minor');
                }

                //when scrolling to section 1 has ended 
                if (index == '1') {
                    $('.nav_wrapper').removeClass('minor');
                }



                if (anchorLink == 'contacts') {                   
                    var $loader = $('.loader');

                    if (typeof ymaps != 'undefined') return;
                        $.getScript('http://api-maps.yandex.ru/2.1/?lang=ru_RU', function () {
                            ymaps.ready(init);
                            $loader.css('background-image', 'none').fadeOut(3000);
                            var myMap;

                            function init() {
                                myMap = new ymaps.Map("altlan_map", {
                                    center: [55.811032, 37.624444],
                                    zoom: 16,
                                    controls: ["zoomControl", "typeSelector"],
                                    behaviors: ['ruler']
                                });

                                myBalloon = new ymaps.Placemark([55.811032, 37.624444], {
                                    balloonContentBody: "<strong>ЗАО «Альт-Лан»</strong> <br/>г. Москва, Звёздный бульвар 21, строение 1, офис 711"
                                });
                                myMap.geoObjects.add(myBalloon);
                                myMap.balloon.open([55.811032, 37.624444], "<strong>ЗАО «Альт-Лан»</strong> <br/>г. Москва, Звёздный бульвар 21, строение 1, офис 711", {
                                    // Опция: не показываем кнопку закрытия.
                                    closeButton: false
                                });
                            }
                        });
                   
                }
            },
            
       });
        //$('html').removeAttr('style')
    } catch (e) {
        console.log('smth gone wrong with fullpage')
    }

   
    $("body").show().animate({ "opacity": "1" }, 1500);  //fade the first div.section in on page load

    $(window).resize(function () {
        if ($(window).width() > 900) setDecoAngles();
    });

    function setIsotop() {
        $('#source').quicksand($('#destination article'), function () {
            console.log('done')
        });
    }

    function setDecoAngles() {
        var a = $(window).width() + 365,
            b = $(".deco_angle.inner").css("bottom"),
            c = 1850;

        if ($(window).width() < 900) {
            $(".deco_angle.outer").css("border-left-width", c);
            $(".deco_angle.inner").css("border-left-width", c);
        } else {
            $(".deco_angle.outer").css("border-left-width", a);
            $(".deco_angle.inner").css("border-left-width", a);
        }

        $(".back").css("height", b);
    }

    function setImgTransition() {
        $('.fadein').each(function () {
            var img = $(this).find('img')
            var std = img.attr("src");
            var hover = std.replace("mono", "colored");
            img.clone().insertBefore(img).attr('src', hover).attr('class', 'colored')
            $('.colored').css({
                position: 'absolute',
                opacity: 0
            });
            $(this).mouseenter(function () {
                img.stop().fadeTo(600, 0);
                img.prev().fadeTo(400, 1);
            }).mouseleave(function () {
                img.stop().fadeTo(400, 1);
                img.prev().fadeTo(600, 0);
            });
        });
    }    

    function setTabs() {
        var tabs = $('.tabs'),
        controls = tabs.find('.projects_slider_controls li'),
          slides = tabs.find('.projects_slides li');

        controls.first().addClass('current');
        slides.first().addClass('current').fadeIn();
       
        $('a', controls).each(function (i) {
            $(this).attr('data-show_slide', i);
            $($(this).closest('.tabs').find('.projects_slides li')[i]).attr('data-targeted_slide', i);
        });           

        $('a', controls).each(function (i) {
            $(this).on('click', function () {
                $(controls).removeClass('current')
                $(this).parent().addClass('current');
                              
                $('.projects_slides li').removeClass('current').hide();
                $('.tabs').find('[data-targeted_slide="' + $(this).attr('data-show_slide') + '"]').fadeIn(1000).addClass('current');
            })
        })

    }

});//end document.ready


