# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0005_auto_20150323_1942'),
    ]

    operations = [
        migrations.RenameField(
            model_name='projectparagraph',
            old_name='text_paragraph',
            new_name='text',
        ),
    ]
