# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='preview',
            field=tinymce.models.HTMLField(default=b'', max_length=385, verbose_name='\u0410\u043d\u043e\u043d\u0441 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430/\u0443\u0441\u043b\u0443\u0433\u0438. \u0414\u043b\u0438\u043d\u0430: \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 385 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='productparagraph',
            name='text_paragraph',
            field=tinymce.models.HTMLField(verbose_name='\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430/\u0443\u0441\u043b\u0443\u0433\u0438', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='project',
            name='preview',
            field=tinymce.models.HTMLField(default=b'', max_length=385, verbose_name='\u0410\u043d\u043e\u043d\u0441 \u043f\u0440\u043e\u0435\u043a\u0442\u0430. \u0414\u043b\u0438\u043d\u0430: \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 385 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='projectparagraph',
            name='text_paragraph',
            field=tinymce.models.HTMLField(verbose_name='\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430/\u0443\u0441\u043b\u0443\u0433\u0438', blank=True),
            preserve_default=True,
        ),
    ]
