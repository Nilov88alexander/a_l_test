# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0006_auto_20150324_1555'),
    ]

    operations = [
        migrations.AddField(
            model_name='productparagraph',
            name='text_image',
            field=tinymce.models.HTMLField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0437\u0435\u043d\u0438\u044f', blank=True),
            preserve_default=True,
        ),
    ]
