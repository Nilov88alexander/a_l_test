# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0010_auto_20150401_1743'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productparagraph',
            name='image',
            field=models.ImageField(default=b'', upload_to=b'photos/product/paragraph', null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
    ]
