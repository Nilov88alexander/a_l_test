# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=250, verbose_name='\u0418\u043c\u044f \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430/\u0443\u0441\u043b\u0443\u0433\u0438', blank=True)),
                ('preview', models.CharField(default=b'', max_length=385, verbose_name='\u0410\u043d\u043e\u043d\u0441 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430/\u0443\u0441\u043b\u0443\u0433\u0438. \u0414\u043b\u0438\u043d\u0430: \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 385 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432', blank=True)),
                ('url', models.CharField(max_length=250, verbose_name='\u041e\u0442\u043d\u043e\u0441\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043f\u0443\u0442\u044c')),
                ('weight', models.IntegerField(default=0, null=True, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043a\u043e\u0432\u044b\u0439 \u043d\u043e\u043c\u0435\u0440', blank=True)),
                ('keywords', models.CharField(default=b'', max_length=250, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 (\u0447\u0435\u0440\u0435\u0437 \u043f\u0440\u043e\u0431\u0435\u043b)', blank=True)),
                ('slug', models.SlugField()),
                ('image', models.ImageField(upload_to=b'photos', null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0434\u0443\u043a\u0442/\u0443\u0441\u043b\u0443\u0433\u0430',
                'verbose_name_plural': '\u041f\u0440\u043e\u0434\u0443\u043a\u0442\u044b/\u0443\u0441\u043b\u0443\u0433\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductParagraph',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text_paragraph', models.TextField(verbose_name='\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430/\u0443\u0441\u043b\u0443\u0433\u0438', blank=True)),
                ('weight', models.IntegerField(default=0, null=True, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043a\u043e\u0432\u044b\u0439 \u043d\u043e\u043c\u0435\u0440', blank=True)),
                ('image', models.ImageField(upload_to=b'photos', null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('product', models.ForeignKey(related_name='+', to='services.Product')),
            ],
            options={
                'verbose_name': '\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444 \u043a \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0443/\u0443\u0441\u043b\u0443\u0433\u0435',
                'verbose_name_plural': '\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444\u044b \u043a \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0443/\u0443\u0441\u043b\u0435\u0433\u0435',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=250, verbose_name='\u0418\u043c\u044f \u043f\u0440\u043e\u0435\u043a\u0442\u0430', blank=True)),
                ('preview', models.TextField(default=b'', max_length=385, verbose_name='\u0410\u043d\u043e\u043d\u0441 \u043f\u0440\u043e\u0435\u043a\u0442\u0430. \u0414\u043b\u0438\u043d\u0430: \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 385 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432', blank=True)),
                ('url', models.CharField(max_length=250, null=True, verbose_name='\u041e\u0442\u043d\u043e\u0441\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043f\u0443\u0442\u044c')),
                ('weight', models.IntegerField(default=0, null=True, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043a\u043e\u0432\u044b\u0439 \u043d\u043e\u043c\u0435\u0440', blank=True)),
                ('keywords', models.CharField(default=b'', max_length=250, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 (\u0447\u0435\u0440\u0435\u0437 \u043f\u0440\u043e\u0431\u0435\u043b)', blank=True)),
                ('slug', models.SlugField()),
                ('image', models.ImageField(upload_to=b'photos', null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0435\u043a\u0442',
                'verbose_name_plural': '\u041f\u0440\u043e\u0435\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectParagraph',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text_paragraph', models.TextField(verbose_name='\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430/\u0443\u0441\u043b\u0443\u0433\u0438', blank=True)),
                ('weight', models.IntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043a\u043e\u0432\u044b\u0439 \u043d\u043e\u043c\u0435\u0440', blank=True)),
                ('image', models.ImageField(upload_to=b'photos', null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('project', models.ForeignKey(related_name='+', to='services.Project')),
            ],
            options={
                'verbose_name': '\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444 \u043a \u043f\u0440\u043e\u0435\u043a\u0442\u0443',
                'verbose_name_plural': '\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444\u044b \u043a \u043f\u0440\u043e\u0435\u043a\u0442\u0443',
            },
            bases=(models.Model,),
        ),
    ]
