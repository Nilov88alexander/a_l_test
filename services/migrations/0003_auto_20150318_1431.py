# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0002_auto_20150317_1626'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ('order',), 'verbose_name': '\u041f\u0440\u043e\u0434\u0443\u043a\u0442/\u0443\u0441\u043b\u0443\u0433\u0430', 'verbose_name_plural': '\u041f\u0440\u043e\u0434\u0443\u043a\u0442\u044b/\u0443\u0441\u043b\u0443\u0433\u0438'},
        ),
        migrations.AlterModelOptions(
            name='productparagraph',
            options={'ordering': ('order',), 'verbose_name': '\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444 \u043a \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0443/\u0443\u0441\u043b\u0443\u0433\u0435', 'verbose_name_plural': '\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444\u044b \u043a \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0443/\u0443\u0441\u043b\u0435\u0433\u0435'},
        ),
        migrations.AlterModelOptions(
            name='project',
            options={'ordering': ('order',), 'verbose_name': '\u041f\u0440\u043e\u0435\u043a\u0442', 'verbose_name_plural': '\u041f\u0440\u043e\u0435\u043a\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='projectparagraph',
            options={'ordering': ('order',), 'verbose_name': '\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444 \u043a \u043f\u0440\u043e\u0435\u043a\u0442\u0443', 'verbose_name_plural': '\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444\u044b \u043a \u043f\u0440\u043e\u0435\u043a\u0442\u0443'},
        ),
        migrations.RenameField(
            model_name='product',
            old_name='weight',
            new_name='order',
        ),
        migrations.RenameField(
            model_name='productparagraph',
            old_name='weight',
            new_name='order',
        ),
        migrations.RenameField(
            model_name='productparagraph',
            old_name='text_paragraph',
            new_name='text',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='weight',
            new_name='order',
        ),
        migrations.RenameField(
            model_name='projectparagraph',
            old_name='weight',
            new_name='order',
        ),
    ]
