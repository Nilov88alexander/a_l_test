# coding=utf-8

import models

from AltLan.functions import get_paginator, \
    context_single_functions_order_by_event_date, \
    context_single_functions_order_by_order


# Create your views here.

class ContactInfoMixin(object):

    def get_contact_info(self):
        contact_info = models.ContactInfo.objects.all()
        return contact_info


    def get_contact_info_context(self):
        context = {'contact_info': self.get_contact_info().get(pk=1)}
        self.context.update(context)

class TeamMixin(object):

    def get_team_all(self):
        team_all = models.Team.objects.all()
        return team_all

class MainTeamMixin(TeamMixin):

    def get_team_context(self):
        team = self.get_team_all().filter(main = 'True')[:5]
        context = {'team_context': team}
        self.context.update(context)
        return self.context


class PartnerMixin(object):

    def get_partners_all(self):
        partners_all = models.Partner.objects.all().order_by('order')
        return partners_all

class MainPartnerMixin(PartnerMixin):
    """
    docstring for MainPartnerMixin:
    Класс описывает вывод логику вывода контента партнеров на главную страницу.
    """
    def get_partners_context(self):
        main_partner = self.get_partners_all().filter(main = True)[:9]
        partner = main_partner[:3]
        partner_logo_mono = main_partner[3:]
        context = {'partner_context': partner, 'partner_logo_mono_context': partner_logo_mono,}
        self.context.update(context)
        return self.context


class AllPartnerMixin(PartnerMixin):

    def get_partners_context(self,page_id, pagination):
        partner_context = get_paginator(self.get_partners_all(), page_id, pagination)
        context= {'page_context':partner_context,}
        self.context.update(context)

class SinglePartnerMixin(PartnerMixin):

    def get_partners_context(self, page_id=1):

        context_single, context_next, context_previous = context_single_functions_order_by_order(self.get_partners_all(), page_id)

        context = {'page_context':context_single,
                'next_context':context_next,
                'previous_context':context_previous}
        self.context.update(context)

class MainTeamTextMixin(ContactInfoMixin):

    def get_team_text(self):
        text_query = self.get_contact_info()

        text = text_query.values('team_text')[0]['team_text']
        if text and ' ' in text:
            text_len = len(text)
            text_left = text[:text_len/2]
            text_right = text[text_len/2:]
            text_left_ind = text_left.rindex(' ')
            text_right_ind = text_right.index(' ')
            if text_len/2 - text_left_ind > text_right_ind:
                text_left = text[:text_len/2+text_right_ind]
                text_right = text[text_len/2+text_right_ind:]
            else:
                text_left = text[:text_left_ind]
                text_right = text[text_left_ind:]
        else:
            text_left = 'Команда вседа на высоте.'
            text_right = 'Команда больших профессионалов'

        context = {"team_text_left":text_left,
                   "team_text_right":text_right}
        self.context.update(context)
