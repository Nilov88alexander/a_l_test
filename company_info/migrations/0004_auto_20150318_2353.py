# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0003_auto_20150318_1431'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactinfo',
            name='address',
            field=models.CharField(default='\u0417\u0432\u0435\u0437\u0434\u043d\u044b\u0439 \u0431\u0443\u043b\u044c\u0432\u0430\u0440\n \u0434\u043e\u043c 21\n \u0441\u0442\u0440\u043e\u0435\u043d\u0438\u0435 1\n \u043e\u0444\u0438\u0441 711', max_length=250, verbose_name='\u0410\u0434\u0440\u0435\u0441'),
            preserve_default=True,
        ),
    ]
