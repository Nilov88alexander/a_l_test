# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_components', '0006_auto_20150325_1731'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slider',
            name='text',
            field=models.CharField(max_length=385, verbose_name='\u0422\u0435\u043a\u0441\u0442 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430. \u0414\u043b\u0438\u043d\u0430: \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 385 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432', blank=True),
            preserve_default=True,
        ),
    ]
