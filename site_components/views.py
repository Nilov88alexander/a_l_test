# -*- coding: utf-8 -*-
import models
from company_info.models import ContactInfo

class MenuMixin(object):
    def __init__(self):
        self.context = {}

    def get_menu_context(self):
        menu = list(models.Menu.objects.all().order_by("order"))
        context= {'menu':menu,}
        self.context.update(context)
        return self.context

class SliderMixin(object):

    def get_slides_context(self):
        slider = list(models.Slider.objects.all().order_by("order"))
        context= {'slider':slider,}
        self.context.update(context)

class HeaderFooterMixin(MenuMixin):

    def get_contact_info_context(self):
        self.context["contact_info"] = list(ContactInfo.objects.all())

    def get_header_footer_context(self):
        self.get_menu_context()
        self.get_contact_info_context()
        return self.context


class SiteDivisionMixin(object):

    def get_division_context(self, mark):
        try:
            division = models.SiteDivision.objects.all().get(mark=mark)
            context= {'division':division,}
        except:
            context = {'division':{'slogan':'',
                                        'text':''}}
        self.context.update(context)

class MainHeaderFooterMixin(MenuMixin, SliderMixin):
    def get_main_header_footer_context(self):
        self.get_menu_context()
        self.get_slides_context()
        context = {"contact_info":ContactInfo.objects.get(pk=1)}
        self.context.update(context)